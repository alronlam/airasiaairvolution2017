import abc
import re

CSV_COLUMNS = ["post_id", "comment_id", "created_time", "from_id", "from_name", "like_count", "comment_count", "tag_count", "message"]

class Rule(object):
    def __init__(self, category):
        self.category = category

    @abc.abstractmethod
    def test(self, params):
        """
        :return:
        """

class TagRule(Rule):

    def __init__(self, category="tag"):
        self.category = category

    def test(self, params):
        if params[CSV_COLUMNS.index("tag_count")] > 0:

class FindTerm(Rule):

    def __init__(self, term_list, category="complaint_general"):
        self.term_list = term_list
        self.category=category

    def test(self, params):
        text = params["message"]
        words = text.split()
        for word in words:
            if word in self.term_list:
                return True
        return False

class DelayRule(Rule):

    def __init__(self, category="delay"):
        self.category = category

    def test(self, params):
        print(params)
        if re.match("(\s|^)([Dd]elay)y*(s|ed|ing|[\s.,!?])", params[CSV_COLUMNS.index("message")]):
            return True
        return False
