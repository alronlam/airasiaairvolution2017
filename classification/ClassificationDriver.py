import csv

from classification.Rule import *
from parsing import CSVParser


def classify(params, rules):
    for rule in rules:
        if rule.test(params):
            return rule.category
    return "Others"

def write_to_csv_file(csv_rows, file):
    writer = csv.writer(file)
    writer.writerows(csv_rows)

def main():

    rules = {DelayRule()}
    # TODO
    # load csv
    input_file = open("input.csv", "r")
    csv_rows = CSVParser.parse_file_into_csv_row_generator(input_file, False)

    # for each csv_row, classify, and add info to row
    out_csv_rows = []
    for csv_row in csv_rows:
        category = classify(csv_row, rules)
        csv_row.append(category)
        out_csv_rows.append(csv_row)

    # save new csv with classifications
    output_file = open("output.csv", "w")
    write_to_csv_file(out_csv_rows, output_file)

main()