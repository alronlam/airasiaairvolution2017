def load_words_from_file(path):
    with open(path, "r") as function_words_file:
        words = [word.strip() for word in function_words_file.readlines()]
    return words
