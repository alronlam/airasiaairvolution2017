from preprocessing import PreProcessing
from topic_modelling.LDATopicModeller import LDATopicModeller


def load_all_texts():
    pass

def preprocess_texts(texts):
    preprocessors = [] #TODO

    return PreProcessing.preprocess_strings(texts, preprocessors)

def model_topics(texts):
    topic_modeller = LDATopicModeller()
    return topic_modeller.model_topics(texts)

def display_topic_models(topic_models):
    topic_string = []
    for index, topic_probability_list in topic_models:
        topic_string.append("Topic {}".format(index))
        for topic, probability in topic_probability_list:
            topic_string.append("{} - {}".format(topic, probability))
        topic_string.append("\n")
    return "\n".join(topic_string)

def main():
    texts = load_all_texts()
    texts = preprocess_texts(texts)
    topic_models = model_topics(texts)
    display_topic_models(topic_models)