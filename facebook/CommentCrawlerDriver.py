import copy
import csv
from datetime import datetime

import re
import requests
import json

app_id = '399521557068765'
app_secret = 'd3e99b926133744580bb78245e228a03'
version = 'v2.8'
JAN_1_2017 = 1483228800
MAR_18_2017 = 1489795200
MAR_1_2017 = 1488326400
#got from Graph API Explorer
access_token = 'EAACEdEose0cBAC5Q2xJxOljOKITwWsYASXuYn3ywZB8j8a1O2J2cSF4ZAu2VPVp2OpRU6wrmc7kLXA06nIch0b2pSf9yiOsBvaAXjn4qVwtdg2KKkZAi2WZB0RbX2D0ViciFCH5ykh1k9J8GgGiMAPc1tWSPoaJk7tbCQJwdsQz0J88MS2rZCdNsHfPJCAEoZD'

payload = {'access_token':access_token}


# Get all posts from page

def get_index_where_first_created_time_lt(data_list, since_datetime):
    for index, data in enumerate(data_list):
        if datetime(data["created_time"]) < since_datetime:
            return index

def get_url_all_pages(url, all_data):
    raw = json.JSONDecoder().decode(requests.get(url,params=payload).text)
    print(raw)
    all_data.extend(raw["data"])

    if "paging" in raw and "next" in raw["paging"]:
        return get_url_all_pages(raw["paging"]["next"], all_data)

    return all_data

def get_all_posts_by_page(page_id, since=1451606400):
    url = 'https://graph.facebook.com/'+version+'/'+page_id+'/posts?since={}'.format(since)
    all_data = get_url_all_pages(url, [])
    return all_data

def get_all_posts_on_page(page_id, since=1451606400):
    url = 'https://graph.facebook.com/'+version+'/'+page_id+'/feed?since={}&fields=message,created_time,from,reactions.summary(1),comments'.format(since)
    all_data = get_url_all_pages(url, [])
    return all_data


# For all posts get comments
def get_all_comments_from_post(post_id):
    url = 'https://graph.facebook.com/'+version+'/'+post_id+'/comments?fields=message,created_time,comment_count,like_count,from,message_tags'
    all_data = get_url_all_pages(url, [])
    return all_data

def initialize_comments_for_posts(posts):
    posts = copy.deepcopy(posts)
    for post in posts:
        comments = get_all_comments_from_post(post["id"])
        for comment in comments:
            comment["likes"] = 0 #TODO
            comment["sub_comments"] = 0 #TODO
            comment["post_id"] = post["id"]
        post["comments"] = comments
    return posts

def generate_csv_rows_for_by_posts(posts):

    name_regex = re.compile("^([A-Z][a-z]*.?)(\s[A-Z][a-z]*.?)*$")

    csv_rows = []
    for post in posts:
        comments = post["comments"]
        for comment in comments:
            csv_rows.append(generate_csv_row_for_comment(comment, post["id"]))
    return csv_rows

def generate_csv_row_for_comment(comment, post_id=None):
    like_count = comment["like_count"] if "like_count" in comment else 0
    comment_count = comment["comment_count"] if "comment_count" in comment else 0
    message = comment["message"] if "message" in comment else ""
    tag_count = len(comment["message_tags"])if "message_tags" in comment else 0
    comment_row = [post_id, comment["id"], comment["created_time"], comment["from"]["id"], comment["from"]["name"],like_count, comment_count, tag_count, message]
    comment_row = [str(x) for x in comment_row]
    return comment_row

def generate_csv_row_for_to_post(post):
    like_count = post["reactions"]["summary"]["total_count"] if "reactions" in post else 0
    comment_count = len(post["comments"]["data"]) if "comments" in post else 0
    message = post["message"] if "message" in post else ""
    tag_count = len(post["message_tags"])if "message_tags" in post else 0
    comment_row = [post["id"], None, post["created_time"], post["from"]["id"], post["from"]["name"], like_count, comment_count, tag_count, message]
    comment_row = [str(x) for x in comment_row]
    return comment_row


def initialize_like_comment_counts_for_to_posts(posts):
    posts = copy.deepcopy(posts)
    for post in posts:
        post["like_count"] = post["reactions"]["summary"]["total_count"]
        post["comment_count"] = 0
    return posts


def write_to_csv_file(csv_rows, file):
    writer = csv.writer(file)
    writer.writerows(csv_rows)

def main_posts_by_airasia():
    posts = get_all_posts_by_page("airasiaph", since=1489622400)
    posts = initialize_comments_for_posts(posts)
    csv_rows = generate_csv_rows_for_by_posts(posts)
    write_to_csv_file(csv_rows, file=open("fb_comments_on_posts_by_page.csv", "w", newline='', encoding="utf8"))


def main():

    TARGET_PAGE = "airasia"

    csv_file = open("fb_comments_on_page_malaysia.csv", "w", newline='', encoding="utf8")

    posts = get_all_posts_on_page(TARGET_PAGE, since=JAN_1_2017)

    # Filter: all by AirAsia
    by_airasia = [post for post in posts if post["from"]["name"].lower() == TARGET_PAGE]
    by_airasia = initialize_comments_for_posts(by_airasia)
    csv_rows = generate_csv_rows_for_by_posts(by_airasia)
    write_to_csv_file(csv_rows, csv_file)

    print(len(csv_rows))

    # Filter: all to AirAsia
    to_airasia = [post for post in posts if post["from"]["name"].lower() != TARGET_PAGE]
    csv_rows = []
    for post in to_airasia:
        csv_rows.append(generate_csv_row_for_to_post(post))
    write_to_csv_file(csv_rows, csv_file)

    print(len(csv_rows))

main()
