from sklearn import metrics
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline

categories = ['alt.atheism', 'soc.religion.christian', 'comp.graphics', 'sci.med']

##### DATA #####

from sklearn.datasets import fetch_20newsgroups
twenty_train = fetch_20newsgroups(subset='train',categories=categories, shuffle=True, random_state=42)
print(len(twenty_train.data))
print(len(twenty_train.filenames))

twenty_test = fetch_20newsgroups(subset='test', categories=categories, shuffle=True, random_state=42)

##### PIPELINE #####
text_clf = Pipeline([('vect', CountVectorizer()),
                            ('tfidf', TfidfTransformer()),
                            ('clf', MultinomialNB())])

text_clf.fit(twenty_train.data, twenty_train.target)
predicted = text_clf.predict(twenty_test.data)
print(metrics.classification_report(twenty_test.target, predicted, target_names=twenty_test.target_names))